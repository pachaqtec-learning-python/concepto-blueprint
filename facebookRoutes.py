from facebookController import FacebookController
from flask import Blueprint

facebookBP = Blueprint('facebook', __name__)
facebook = FacebookController()

@facebookBP.route('/')
def index():
    return facebook.validate()