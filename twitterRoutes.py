from twitterController import TwitterController
from flask import Blueprint

twitterBP = Blueprint('twitter', __name__)
twitter = TwitterController()

@twitterBP.route('/')
def index():
    return twitter.validate()