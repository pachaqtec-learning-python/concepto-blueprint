from flask import Flask

def create_app():
    app = Flask(__name__)

    from facebookRoutes import facebookBP
    app.register_blueprint(facebookBP, url_prefix='/facebook')

    from twitterRoutes import twitterBP
    app.register_blueprint(twitterBP, url_prefix='/twitter')

    return app

if __name__ == '__main__':
    app = create_app()
    app.run(port=8000, debug=True)
